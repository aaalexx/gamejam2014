﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class SBoard : MonoBehaviour
{
    enum State : int
    {
        IDLE,
        MOVING,
        MATCHING,
        EXPLOSION,
        REFILL
    }

    enum MoveDirection : int
    {
        UP = 0,
        DOWN,
        LEFT,
        RIGHT
    }

    public TextAsset Level;
    public float ANIM_SPEED = 1;
    public float SPRITE_RATIO = 1;
    public int Rows = 5;
    public int Cols = 5;
    public GameObject TilePrefab;
    public Texture2D EmptyTexture;
    public Texture2D TileTexture;

    public AudioClip SoundInsert;
    public AudioClip SoundMatch;
    public AudioClip SoundNoMatch;
    public AudioClip SoundExplosion;
    public AudioClip SoundRefill;

    public static int mMoves = 60;
    public static int mObjectives = 10;
    public static int mMaxObjectives = 10;
    GameObject mTilesParent = null;
    private CBoard mBoard;
    private GameObject[,] mTilesObj;
    private GameObject mSelTile = null;
    private GameObject mInsideTile = null;
    private bool mIsMoving = false;
    private GameObject mExtraTile = null;
    private float delta = 0;
    private State mState = State.IDLE;
    private TileColor mExtraTileColor;
    private MoveDirection mLastDirection;
    public static float mScore = 0;
    private int mNumberOfCombos = 0;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;

        mBoard = new CBoard(Rows + 2, Cols + 2, Level);
        mTilesObj = new GameObject[Rows + 2, Cols + 2];
        mTilesParent = new GameObject("TilesObjects");

        // create extra tile
        if (mExtraTile == null)
        {
            CTile newTile = mBoard.GetTile(0, 0);
            newTile = new CTile(TileColor.RED, new Vector2(-1, -1));
            mTilesObj[0, 0] = CreateTileObject(mTilesParent, newTile);
            mExtraTileColor = TileColor.RED;
            int row = 0;
            int col = 0;
            mExtraTile = mTilesObj[row, col];

            SetTileObjOnPos(mExtraTile, row, col);
        }

        for (int i = 0; i < Cols + 2; i++)
        {
            for (int j = 0; j < Rows + 2; j++)
            {
                if (i == 0 && j == 0 ||
                    i == 0 && j == Rows + 1 ||
                    i == Cols + 1 && j == 0 ||
                    i == Cols + 1 && j == Rows + 1)
                {
                    continue;
                }

                GameObject tile = null;
                if (i == 0 || j == 0 || i == Cols + 1 || j == Rows + 1)
                {
                    tile = CreateTileGrid(i, j, "Tile_Empty_", true);
                }
                else
                {
                    tile = CreateTileGrid(i, j, "Tile_");
                    mTilesObj[i, j] = CreateTileObject(mTilesParent, mBoard.GetTile(i, j));
                }

                SetTileObjOnPos(tile, i, j);
                SetTileObjOnPos(mTilesObj[i, j], i, j);

//                float deltaX = tile.transform.localScale.x;
//                float deltaY = tile.transform.localScale.y;
//                Vector2 newPosition = new Vector2(deltaX / 2 + deltaX * j, 1 - deltaY / 2 - deltaY * i);
//                tile.transform.position = newPosition;
//                if (mTilesObj[i, j] != null)
//                {
//                    mTilesObj[i, j].transform.position = newPosition;
//                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
//        Debug.Log("state " + mState);

        switch (mState)
        {
            case State.IDLE:
            case State.MOVING:
                UpdateInput();
                break;
            case State.MATCHING:
                UpdateMatch();
                break;
            case State.EXPLOSION:
                UpdateExplosion();
                break;
            case State.REFILL:
                UpdateRefill();
                break;
        }
    }

    void UpdateInput()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject tileGrid = transform.GetChild(i).gameObject;
            if (!mIsMoving && RCCustomInput.IsClickPressed())
            {
                STile sTile = tileGrid.GetComponent<STile>();
                if (sTile != null && tileGrid.guiTexture != null && sTile.IsEmptyTile &&
                    tileGrid.guiTexture.HitTest(RCCustomInput.GetCursorPostion_V3()))
                {
                    mSelTile = tileGrid;
//					tileGrid.guiTexture.texture = mExtraTile.guiTexture.texture;
					if (mExtraTileColor == TileColor.RED)
					{
						tileGrid.guiTexture.texture = Resources.Load<Texture2D>("Objects/red_penguin");
					}
					else if (mExtraTileColor == TileColor.BLUE)
					{
						tileGrid.guiTexture.texture = Resources.Load<Texture2D>("Objects/blue_penguin");
					}
					else if (mExtraTileColor == TileColor.GREEN)
					{
						tileGrid.guiTexture.texture = Resources.Load<Texture2D>("Objects/green_penguin");
					}
					else if (mExtraTileColor == TileColor.PURPLE)
					{
						tileGrid.guiTexture.texture = Resources.Load<Texture2D>("Objects/purple_penguin");
					}
					else
					{
						tileGrid.guiTexture.texture = Resources.Load<Texture2D>("Objects/yellow_penguin");
					}
                    break;
                }
                else
                {
					tileGrid.guiTexture.enabled = false;
                    mSelTile = null;
                }
                if (sTile != null && tileGrid.guiTexture != null && !sTile.IsEmptyTile && 
                    tileGrid.guiTexture.HitTest(RCCustomInput.GetCursorPostion_V3()))
                {
                    mInsideTile = tileGrid;
                    break;
                }
                else
                {
                    mInsideTile = null;
                }
            }
            else
            {
                if (mInsideTile != null)
                {
                    STile sInsideTile = mInsideTile.GetComponent<STile>();
                    if (mBoard.IsPowerUp(sInsideTile.Row, sInsideTile.Col))
                    {
                        if (mBoard.GetTile(sInsideTile.Row, sInsideTile.Col).Color == TileColor.POWERUP_1)
                        {
                            MarkForDeleteForPowerUp(sInsideTile.Row, sInsideTile.Col, 1);
                        }
                        if (mBoard.GetTile(sInsideTile.Row, sInsideTile.Col).Color == TileColor.POWERUP_2)
                        {
                            MarkForDeleteForPowerUp(sInsideTile.Row, sInsideTile.Col, 2);
                        }
                        if (mBoard.GetTile(sInsideTile.Row, sInsideTile.Col).Color == TileColor.POWERUP_3)
                        {
                            MarkForDeleteForPowerUp(sInsideTile.Row, sInsideTile.Col, 3);
                        }
                        if (mBoard.GetTile(sInsideTile.Row, sInsideTile.Col).Color == TileColor.POWERUP_4)
                        {
                            MarkForDeleteForPowerUp(sInsideTile.Row, sInsideTile.Col, 4);
                        }

                        StartExplosion(true);
                    }
                }
                if (mSelTile != null)
                {
                    if (mState != State.MOVING)
                    {
                        int sndId = Random.Range(1, 4);
                        string sndPath = "Sounds/Penguin_Placement_" + sndId;
                        audio.clip = Resources.Load<AudioClip>(sndPath);
                        audio.Play();
                    }

                    mState = State.MOVING;
                    STile sSelTile = mSelTile.GetComponent<STile>();
                    //                    Debug.Log("action " + sSelTile.Row + "," + sSelTile.Col + " -> " + mSelTile.name);
                    
                    if (sSelTile.Col == 0)
                    {
                        MoveTileObj(MoveDirection.RIGHT, sSelTile.Row, sSelTile.Col);
                    }
                    else if (sSelTile.Col == Cols + 1)
                    {
                        MoveTileObj(MoveDirection.LEFT, sSelTile.Row, sSelTile.Col);
                    }
                    else if (sSelTile.Row == 0)
                    {
                        MoveTileObj(MoveDirection.DOWN, sSelTile.Row, sSelTile.Col);
                    }
                    else if (sSelTile.Row == Rows + 1)
                    {
                        MoveTileObj(MoveDirection.UP, sSelTile.Row, sSelTile.Col);
                    }
                }
            }
        }
    }

    void MarkForDeleteForPowerUp(int row, int col, int size)
    {
        mBoard.GetTile(row, col).MarkForDelete(true);
        for (int i = 1; i <= size; ++i)
        {
            if (row - i >= 1)
            {
                mBoard.GetTile(row - i, col).MarkForDelete(true);
            }
            if (row + i <= Rows)
            {
                mBoard.GetTile(row + i, col).MarkForDelete(true);
            }
            if (col - i >= 1)
            {
                mBoard.GetTile(row, col - i).MarkForDelete(true);
            }
            if (col + i <= Cols)
            {
                mBoard.GetTile(row, col + i).MarkForDelete(true);
            }
        }
    }

    void UpdateMatch()
    {
        bool matchFound = FindAndRemoveNextMatch();
        if (matchFound)
        {
            StartExplosion();
        }
        else
        {
            int sndId = Random.Range(1, 4);
            string sndPath = "Sounds/Push_Nomatch_" + sndId;
            audio.clip = Resources.Load<AudioClip>(sndPath);
            audio.Play();

            mState = State.IDLE;
        }
    }

    void UpdateExplosion()
    {
        for (int i = 1; i <= Cols; ++i)
        {
            for (int j = 1; j <= Rows; ++j)
            {
                if (mBoard.GetTile(i, j).IsMarkForDelete())
                {
                    if (mTilesObj[i, j].transform.FindChild("NO_BLINK").transform.localScale.x <= 0.125f)
                    {
                        RemoveExtraTiles();
                    }
                    return;
                }
            }
        }
    }

    void UpdateRefill()
    {
        for (int i = 1; i <= Cols; ++i)
        {
            for (int j = 1; j <= Rows; ++j)
            {
                if (mBoard.GetTile(i, j).IsMarkForNew())
                {
                    if (mTilesObj[i, j].transform.FindChild("NO_BLINK").transform.localScale.x == 1)
                    {
                        mState = State.MATCHING;

                        for (int ii = 1; ii <= Cols; ++ii)
                        {
                            for (int jj = 1; jj <= Rows; ++jj)
                            {
                                if (mBoard.GetTile(ii, jj).IsMarkForNew())
                                {
                                    mBoard.GetTile(ii, jj).MarkForNew(false);
                                }
                            }
                        }
                    }
                    return;
                }
            }
        }

        mState = State.MATCHING;
    }

    void StartExplosion(bool isPowerUp = false)
    {
        for (int i = 1; i <= Cols; ++i)
        {
            for (int j = 1; j <= Rows; ++j)
            {
                if (mBoard.GetTile(i, j).IsMarkForDelete())
                {
                    Animator animator = mTilesObj[i, j].GetComponent<Animator>();
                    animator.SetInteger("mID", 10);
                }
            }
        }

        string sndPath = "";
        if (isPowerUp)
        {
            sndPath = "Sounds/BoomBox_Explosion";
        }
        else
        {
            int sndId = Random.Range(1, 7);
            sndPath = "Sounds/Match_" + sndId;
        }

        audio.clip = Resources.Load<AudioClip>(sndPath);
        audio.Play();

        mState = State.EXPLOSION;
    }

    void StartRefill()
    {
        for (int i = 1; i <= Cols; ++i)
        {
            for (int j = 1; j <= Rows; ++j)
            {
                if (mBoard.GetTile(i, j).ShouldBePowerUp() != TileColor.POWERUP_NONE)
                {
                    mBoard.ResetTileWithPowerUp(i, j, mBoard.GetTile(i, j).ShouldBePowerUp());
                    mTilesObj[i, j] = CreateTileObject(mTilesParent, mBoard.GetTile(i, j));
                    SetTileObjOnPos(mTilesObj[i, j], i, j);
                    
                    mBoard.GetTile(i, j).MarkForNew(true);
                    
                    Animator animator = mTilesObj[i, j].GetComponent<Animator>();
                    animator.SetInteger("mID", 20);
                }
                else if (mBoard.GetTile(i, j).IsMarkForDelete())
                {
                    mBoard.ResetTile(i, j);
                    mTilesObj[i, j] = CreateTileObject(mTilesParent, mBoard.GetTile(i, j));
                    SetTileObjOnPos(mTilesObj[i, j], i, j);

                    mBoard.GetTile(i, j).MarkForNew(true);

                    Animator animator = mTilesObj[i, j].GetComponent<Animator>();
                    animator.SetInteger("mID", 20);
                }
            }
        }

        audio.clip = Resources.Load<AudioClip>("Sounds/Penguins_Refill");
        audio.PlayDelayed(0.5f);

        mState = State.REFILL;
    }

    void RemoveExtraTiles()
    {
        for (int i = 1; i <= Cols; ++i)
        {
            for (int j = 1; j <= Rows; ++j)
            {
                if (mBoard.GetTile(i, j).IsMarkForDelete())
                {
                    GameObject tmp = mTilesObj[i, j];
                    mTilesObj[i, j] = null;
                    tmp.SetActive(false);
                    Destroy(tmp);
                }
            }
        }

        StartRefill();
    }

    bool FindAndRemoveNextMatch()
    {
        bool matchFound = false;
        mNumberOfCombos++;
        for (int i = 1; i <= Cols; ++i)
        {
            for (int j = 1; j <= Rows; ++j)
            {
                if (mBoard.GetTile(i, j).IsMarkForDelete())
                {
                    continue;
                }
                List<CTile> chain = new List<CTile>();
                chain.Add(mBoard.GetTile(i, j));
                mBoard.GetTile(i, j).MarkForDelete(true);
                for (int k = 0; k < chain.Count; ++k)
                {
                    CTile currentTile = chain[k];
                    if (currentTile.X > 1 && mBoard.GetTile(currentTile.X - 1, currentTile.Y).Color ==
                        currentTile.Color && !mBoard.GetTile(currentTile.X - 1, currentTile.Y).IsMarkForDelete())
                    {
                        chain.Add(mBoard.GetTile(currentTile.X - 1, currentTile.Y));
                        mBoard.GetTile(currentTile.X - 1, currentTile.Y).MarkForDelete(true);
                    }
                    if (currentTile.X < Rows && mBoard.GetTile(currentTile.X + 1, currentTile.Y).Color ==
                        currentTile.Color && !mBoard.GetTile(currentTile.X + 1, currentTile.Y).IsMarkForDelete())
                    {
                        chain.Add(mBoard.GetTile(currentTile.X + 1, currentTile.Y));
                        mBoard.GetTile(currentTile.X + 1, currentTile.Y).MarkForDelete(true);
                    }
                    if (currentTile.Y > 1 && mBoard.GetTile(currentTile.X, currentTile.Y - 1).Color ==
                        currentTile.Color && !mBoard.GetTile(currentTile.X, currentTile.Y - 1).IsMarkForDelete())
                    {
                        chain.Add(mBoard.GetTile(currentTile.X, currentTile.Y - 1));
                        mBoard.GetTile(currentTile.X, currentTile.Y - 1).MarkForDelete(true);
                    }
                    if (currentTile.Y < Cols && mBoard.GetTile(currentTile.X, currentTile.Y + 1).Color ==
                        currentTile.Color && !mBoard.GetTile(currentTile.X, currentTile.Y + 1).IsMarkForDelete())
                    {
                        chain.Add(mBoard.GetTile(currentTile.X, currentTile.Y + 1));
                        mBoard.GetTile(currentTile.X, currentTile.Y + 1).MarkForDelete(true);
                    }
                }
                
                if (chain.Count < 4)
                {
                    for (int k = 0; k < chain.Count; ++k)
                    {
                        mBoard.GetTile(chain[k].X, chain[k].Y).MarkForDelete(false);
                    }
                }
                else
                {
                    float multiplier = 1 + (mNumberOfCombos - 1) * 0.5f;
                    int s = 0;
                    int score = 0;
                    for (int p = 0; p < chain.Count; ++p)
                    {
                        score += 100 + s;
                        s = s + 10;
                    }
                    mScore = mScore + score * multiplier; 

                    switch (chain.Count)
                    {
                        case 5:
                            mBoard.GetTile(chain[0].X, chain[0].Y).SetShouldBePowerUp(TileColor.POWERUP_1);
                            break;
                        case 6:
                            mBoard.GetTile(chain[0].X, chain[0].Y).SetShouldBePowerUp(TileColor.POWERUP_2);
                            break;
                        case 7:
                            mBoard.GetTile(chain[0].X, chain[0].Y).SetShouldBePowerUp(TileColor.POWERUP_3);
                            break;
                        case 8:
                        case 9:
                            mBoard.GetTile(chain[0].X, chain[0].Y).SetShouldBePowerUp(TileColor.POWERUP_4);
                            break;
                    }

                    matchFound = true;
                }
                chain.Clear();
            }
        }

        return matchFound;
    }
    
    GameObject CreateTileGrid(int row, int col, string name, bool isEmpty = false)
    {
        GameObject tile = new GameObject(name + (row * (Rows + 2) + col));
        tile.AddComponent<GUITexture>();
        tile.guiTexture.texture = isEmpty ? EmptyTexture : TileTexture;
        tile.transform.parent = this.transform;
        
        tile.AddComponent<STile>();
        STile sTile = tile.GetComponent<STile>();
        sTile.IsEmptyTile = isEmpty;
        sTile.Row = row;
        sTile.Col = col;
        
        Vector2 newScale = new Vector2((float) TileTexture.width / Screen.width, (float) TileTexture.height / Screen.height);
        newScale.x /= ((float) TileTexture.width / Screen.width) * (Rows + 2);
        newScale.y /= ((float) TileTexture.width / Screen.width) * (Rows + 2);
        newScale *= SPRITE_RATIO;
        tile.transform.localScale = newScale;
        
        tile.guiTexture.enabled = false;

        return tile;
    }

    GameObject CreateTileObject(GameObject parent, CTile tile)
    {
        GameObject tileParentObj = (GameObject) Instantiate(TilePrefab);

        Texture2D texColor = EmptyTexture;
        switch (tile.Color)
        {
            case TileColor.RED:
                tileParentObj.name = "RED";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/red_penguin");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/red_penguin_blinkframe01");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/red_penguin_blinkframe02");
//                Animator anim = tileParentObj.AddComponent<Animator>();
//                anim.animation["idle_1"].time = Random.Range(0.0f, animation["idle_1"].length);
                break;
            case TileColor.GREEN:
                tileParentObj.name = "GREEN";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/green_penguin");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/green_penguin_blinkframe01");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/green_penguin_blinkframe02");
                break;
            case TileColor.BLUE:
                tileParentObj.name = "BLUE";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/blue_penguin");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/blue_penguin_blinkframe01");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/blue_penguin_blinkframe02");
                break;
            case TileColor.YELLOW:
                tileParentObj.name = "YELLOW";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/yellow_penguin");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/yellow_penguin_blinkframe01");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/yellow_penguin_blinkframe02");
                break;
            case TileColor.PURPLE:
                tileParentObj.name = "PURPLE";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/purple_penguin");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/purple_penguin_blinkframe01");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/purple_penguin_blinkframe02");
                break;
            case TileColor.BEACH_BALL:
                tileParentObj.name = "BEACHBALL";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/beach_ball");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/beach_ball");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/beach_ball");
                break;
            case TileColor.POWERUP_1:
                tileParentObj.name = "POWERUP_1";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level01");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level01");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level01");
                break;
            case TileColor.POWERUP_2:
                tileParentObj.name = "POWERUP_2";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level02");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level02");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level02");
                break;
            case TileColor.POWERUP_3:
                tileParentObj.name = "POWERUP_3";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level03");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level03");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level03");
                break;
            case TileColor.POWERUP_4:
                tileParentObj.name = "POWERUP_4";
                tileParentObj.transform.FindChild("NO_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level04");
                tileParentObj.transform.FindChild("SEMI_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level04");
                tileParentObj.transform.FindChild("FULL_BLINK").guiTexture.texture = Resources.Load<Texture2D>("Objects/loudspeaker_level04");
                break;
        }
        
        tileParentObj.transform.parent = parent.transform;

        Vector2 newScale = new Vector2((float) texColor.width / Screen.width, (float) texColor.height / Screen.height);
        newScale.x /= ((float) texColor.width / Screen.width) * (Rows + 2);
        newScale.y /= ((float) texColor.width / Screen.width) * (Rows + 2);
        newScale *= SPRITE_RATIO;
        tileParentObj.transform.localScale = newScale;

        return tileParentObj;
    }

    void MoveTileObj(MoveDirection direction, int row, int col)
    {
        mIsMoving = true;

        float sizeX = 0, sizeY = 0;
        if (mTilesObj[row, 1])
        {
            sizeX = mTilesObj[row, 1].transform.localScale.x / SPRITE_RATIO;
        }
        if (mTilesObj[1, col])
        {
            sizeY = mTilesObj[1, col].transform.localScale.y / SPRITE_RATIO;
        }

        switch (direction)
        {
            case MoveDirection.UP:
                if (row == Rows + 1 && mExtraTile != null)
                {
                    SetTileObjOnPos(mExtraTile, row, col);
                    mTilesObj[row, col] = mExtraTile;
                    mTilesObj[0, 0] = null;
                    mExtraTile = null;
                }
//                Debug.Log("Move up row " + col);
                delta += ANIM_SPEED * sizeY * Time.deltaTime;
                for (int i = Rows + 1; i > 0; i--)
                {
                    if (delta < sizeY)
                    {
                        Vector2 newPosition = new Vector2();
                        newPosition.x = mTilesObj[i, col].transform.position.x;
                        newPosition.y = mTilesObj[i, col].transform.position.y + ANIM_SPEED * sizeY * Time.deltaTime;
                        mTilesObj[i, col].transform.position = newPosition;
                    }
                    
                    if (delta >= sizeY && i == 1)
                    {
                        mSelTile = null;
                        delta = 0;
                        mIsMoving = false;
                        mExtraTile = mTilesObj[i, col]; 
//                      mExtraTileColor = mBoard.GetTile(i, col).Color;
                        mTilesObj[i, col] = null;
                        mTilesObj[0, 0] = mExtraTile;
                        SetTileObjOnPos(mExtraTile, 0, 0);
                    }
                }
                break;
            case MoveDirection.DOWN:
                if (row == 0 && mExtraTile != null)
                {
                    SetTileObjOnPos(mExtraTile, row, col);
                    mTilesObj[row, col] = mExtraTile;
                    mTilesObj[0, 0] = null;
                    mExtraTile = null;
                }
                
//                Debug.Log("Move down row " + col);
                delta += ANIM_SPEED * sizeY * Time.deltaTime;
                for (int i = row; i < Rows + 1; i++)
                {
                    if (delta < sizeY)
                    {
                        Vector2 newPosition = new Vector2();
                        newPosition.x = mTilesObj[i, col].transform.position.x;
                        newPosition.y = mTilesObj[i, col].transform.position.y - ANIM_SPEED * sizeY * Time.deltaTime;
                        mTilesObj[i, col].transform.position = newPosition;
                    }
                    
                    if (delta >= sizeY && i == Rows)
                    {
                        mSelTile = null;
                        delta = 0;
                        mIsMoving = false;
                        mExtraTile = mTilesObj[i, col];
//                      mExtraTileColor = mBoard.GetTile(i, col).Color;
                        mTilesObj[i, col] = null;
                        mTilesObj[0, 0] = mExtraTile;
                        SetTileObjOnPos(mExtraTile, 0, 0);
                    }
                }
                break;
            case MoveDirection.LEFT:
                if (col == Cols + 1 && mExtraTile != null)
                {
                    SetTileObjOnPos(mExtraTile, row, col);
                    mTilesObj[row, col] = mExtraTile;
                    mTilesObj[0, 0] = null;
                    mExtraTile = null;
                }
                
//                Debug.Log("Move left row " + row);
                delta += ANIM_SPEED * sizeX * Time.deltaTime;
                for (int i = Cols + 1; i > 0; i--)
                {
                    if (delta < sizeX)
                    {
                        Vector2 newPosition = new Vector2();
                        newPosition.x = mTilesObj[row, i].transform.position.x - ANIM_SPEED * sizeX * Time.deltaTime;
                        newPosition.y = mTilesObj[row, i].transform.position.y;
                        mTilesObj[row, i].transform.position = newPosition;
                    }
                    
                    if (delta >= sizeX && i == 1)
                    {
                        mSelTile = null;
                        delta = 0;
                        mIsMoving = false;
                        mExtraTile = mTilesObj[row, i];
//                      mExtraTileColor = mBoard.GetTile(row, i).Color;
                        mTilesObj[row, i] = null;
                        mTilesObj[0, 0] = mExtraTile;
                        SetTileObjOnPos(mExtraTile, 0, 0);
                    }
                }
                break;
            case MoveDirection.RIGHT:
                if (col == 0 && mExtraTile != null)
                {
                    SetTileObjOnPos(mExtraTile, row, col);
                    mTilesObj[row, col] = mExtraTile;
                    mTilesObj[0, 0] = null;
                    mExtraTile = null;
                }
            
//                Debug.Log("Move right row " + row);
                delta += ANIM_SPEED * sizeX * Time.deltaTime;
                for (int i = col; i < Cols + 1; i++)
                {
                    if (delta < sizeX)
                    {
                        Vector2 newPosition = new Vector2();
                        newPosition.x = mTilesObj[row, i].transform.position.x + ANIM_SPEED * sizeX * Time.deltaTime;
                        newPosition.y = mTilesObj[row, i].transform.position.y;
                        mTilesObj[row, i].transform.position = newPosition;
                    }
                
                    if (delta >= sizeX && i == Cols)
                    {
                        mSelTile = null;
                        delta = 0;
                        mIsMoving = false;
                        mExtraTile = mTilesObj[row, i];
//                      mExtraTileColor = mBoard.GetTile(row, i).Color;
                        mTilesObj[row, i] = null;
                        mTilesObj[0, 0] = mExtraTile;
                        SetTileObjOnPos(mExtraTile, 0, 0);
                    }
                }
                break;
        }

        if (mSelTile == null)
        {
            mMoves--;
            mNumberOfCombos = 0;
            mLastDirection = direction;
            switch (direction)
            {
                case MoveDirection.UP:
                    for (int i = 0; i < Rows + 1; i++)
                    {
                        mTilesObj[i, col] = mTilesObj[i + 1, col];
                        mBoard.SetTile(i, col, mBoard.GetTile(i + 1, col));
                    }
                    mTilesObj[Rows + 1, col] = null;
                    mBoard.SetTileColor(Rows, col, mExtraTileColor);
                    mExtraTileColor = mBoard.GetTile(0, col).Color;
                    break;
                case MoveDirection.DOWN:
                    for (int i = Rows + 1; i > 0; i--)
                    {
                        mTilesObj[i, col] = mTilesObj[i - 1, col];
                        mBoard.SetTile(i, col, mBoard.GetTile(i - 1, col));
                    }
                    mTilesObj[0, col] = null;
                    mBoard.SetTileColor(1, col, mExtraTileColor);
                    mExtraTileColor = mBoard.GetTile(Rows + 1, col).Color;
                    break;
                case MoveDirection.LEFT:
                    for (int i = 0; i < Cols + 1; i++)
                    {
                        mTilesObj[row, i] = mTilesObj[row, i + 1];
                        mBoard.SetTile(row, i, mBoard.GetTile(row, i + 1));
                    }
                    mTilesObj[row, Cols + 1] = null;
                    mBoard.SetTileColor(row, Cols, mExtraTileColor);
                    mExtraTileColor = mBoard.GetTile(row, 0).Color;
                    break;
                case MoveDirection.RIGHT:
                    for (int i = Cols + 1; i > 0; i--)
                    {
                        mTilesObj[row, i] = mTilesObj[row, i - 1];
                        mBoard.SetTile(row, i, mBoard.GetTile(row, i - 1));
                    }
                    mTilesObj[row, 0] = null;
                    mBoard.SetTileColor(row, 1, mExtraTileColor);
                    mExtraTileColor = mBoard.GetTile(row, Cols + 1).Color;
                    break;
            }
            mState = State.MATCHING;
            if (mExtraTileColor == TileColor.BEACH_BALL)
            {
                GameObject tmp = mExtraTile;
                mExtraTile = null;
                Destroy(tmp);
                
                CTile newTile = mBoard.GetTile(0, 0);
                newTile = new CTile(TileColor.RED, new Vector2(-1, -1));
                mTilesObj[0, 0] = CreateTileObject(mTilesParent, newTile);
                mExtraTileColor = TileColor.RED;
                mExtraTile = mTilesObj[0, 0];
                
                SetTileObjOnPos(mExtraTile, 0, 0);
                mObjectives--;
            }
        }
    }

    void SetTileObjOnPos(GameObject tileObj, int row, int col)
    {
        if (tileObj != null)
        {
            float deltaX = tileObj.transform.localScale.x / SPRITE_RATIO;
            float deltaY = tileObj.transform.localScale.y / SPRITE_RATIO;
            float startY = (Rows + 2) * deltaY;
            Vector2 newPosition = new Vector2(deltaX / 2 + deltaX * col, startY - deltaY / 2 - deltaY * row);
            tileObj.transform.position = newPosition;
        }
    }
}
