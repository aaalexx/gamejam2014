﻿using UnityEngine;
using System.Collections;

public class CBoard
{
    private CTile[,] mTiles;
    private int mCols;
    private int mRows;

    public CBoard(int rows, int cols, TextAsset textAsset)
    {
        mTiles = new CTile[rows, cols];
        mCols = cols;
        mRows = rows;

        string[] text = null;
        if (textAsset != null)
        {
            text = textAsset.ToString().Split('\n');
        }

        for (int i = 0; i < cols; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                if (i == 0 && j == 0 ||
                    i == 0 && j == rows - 1 ||
                    i == cols - 1 && j == 0 ||
                    i == cols - 1 && j == rows - 1)
                {
                    continue;
                }

                TileColor color;
                if (text != null)
                {
                    color = (TileColor) (int.Parse(text[i][j].ToString()) - 1);
                }
                else
                {
                    color = (TileColor) Random.Range((int) TileColor.RED, (int) TileColor.SIZE);
                }
                mTiles[i, j] = CreateTile(new Vector2(i, j), color);
            }
        }
    }

    CTile CreateTile(Vector2 position, TileColor color)
    {
        return new CTile(color, position);
    }

    public CTile GetTile(int row, int col)
    {
        return mTiles[row, col];
    }

    public void SetTile(int row, int col, CTile tile)
    {
        mTiles[row, col].SetColor(tile.Color);
    }

    public void SetTileColor(int row, int col, TileColor color)
    {
        mTiles[row, col].SetColor(color);
    }

    public void ResetTile(int row, int col, bool beachball = true)
    {
        mTiles[row, col].MarkForDelete(false);
        mTiles[row, col].SetColor((TileColor) Random.Range((int) TileColor.RED, (int) TileColor.SIZE));
        int numberOfBalls = 0;
        for (int i = 1; i <= mRows - 2; ++i)
        {
            for (int j = 1; j <= mCols - 2; ++j)
            {
                if (mTiles[i, j].Color == TileColor.BEACH_BALL)
                {
                    numberOfBalls++;
                }
            }
        }
        if (numberOfBalls < 2 && beachball)
        {
            if (row != 1 && col != 1 && row != mRows - 2 && col != mCols - 2)
            {
                if (Random.Range(0, 100) <= 20)
                {
                    mTiles[row, col].SetColor(TileColor.BEACH_BALL);
                }
            }
        }
    }

    public void ResetTileWithPowerUp(int row, int col, TileColor powerup)
    {
        mTiles[row, col].MarkForDelete(false);
        mTiles[row, col].SetColor(powerup);
        mTiles[row, col].SetShouldBePowerUp(TileColor.POWERUP_NONE);
    }

    public bool IsPowerUp(int row, int col)
    {
        if (mTiles[row, col].Color == TileColor.POWERUP_1 ||
            mTiles[row, col].Color == TileColor.POWERUP_2 ||
            mTiles[row, col].Color == TileColor.POWERUP_3 ||
            mTiles[row, col].Color == TileColor.POWERUP_4)
        {
            return true;
        }

        return false;
    }
}
