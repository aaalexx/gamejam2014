﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// a custom AudioSource
class RCAudioSource
{
	public AudioSource mAudio = null;
	public float mTargetVolume = 1.0f;
	public float mSpeedVolume = 0.0f;

	public void Update()
	{
		if ( mSpeedVolume != 0.0f )
		{
			float newVolume = mAudio.volume + mSpeedVolume * Time.deltaTime;
			if ( ((mSpeedVolume > 0.0f) && (newVolume >= mTargetVolume)) ||
				 ((mSpeedVolume < 0.0f) && (newVolume <= mTargetVolume))
			    )
			{
				newVolume = mTargetVolume;
				mSpeedVolume = 0.0f;
			}

			mAudio.volume = newVolume;

			if ( newVolume == 0.0f )
				mAudio.Stop();
		}
	}

	public void Stop()
	{
		mAudio.Stop();
		mSpeedVolume = 0.0f;
	}
}

// Audio Manager
public class RSAudioManager : MonoBehaviour
{
	private static RSAudioManager instance = null;

	#region PUBLIC VALUES
	public AudioClip BackgroundMusicClip;
	#endregion
	
	float MasterVolume = 1.0f;
	bool AllSoundsAreOff = false;

	RCAudioSource mBackgroundMusic = null;
	List<RCAudioSource> m_listSounds = new List<RCAudioSource>();

	AudioClip mNewBackgroundMusicClip = null;
	float mNewBackgroundMusicFadeInTime = 0.0f;
	bool mNewBackgroundMusicLoop = true;
	float mNewBackgroundMusicVolume = 0.0f;

	// ---------------------------

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		mBackgroundMusic = AddRCAudioSource();

		// start with a few AudioSources added ( lets say 5 )
		RCAudioSource newRCAS = null;
		for(int i=0; i<5; i++)
		{
			newRCAS = AddRCAudioSource();
			m_listSounds.Add( newRCAS );
		}
	}

	void Update()
	{
		// update audio sources
		mBackgroundMusic.Update();

		for(int i=0; i<m_listSounds.Count; i++)
			m_listSounds[i].Update();

		// if a new background music is waiting to start
		if ( mNewBackgroundMusicClip != null )
		{
			// if the current background music has finished fading
			if ( mBackgroundMusic.mSpeedVolume == 0.0f )
			{
				StartBackgroundMusic( mNewBackgroundMusicClip, mNewBackgroundMusicLoop, mNewBackgroundMusicVolume, mNewBackgroundMusicFadeInTime );
				mNewBackgroundMusicClip = null;
			}
		}
	}

	#region PUBLIC METHODS

	public void SetMasterVolume(float volume)
	{
		MasterVolume = volume;

		// update all audio sources volumes

		mBackgroundMusic.mAudio.volume = MasterVolume;
		mBackgroundMusic.mSpeedVolume = 0.0f;

		for(int i=0; i<m_listSounds.Count; i++)
		{
			m_listSounds[i].mAudio.volume = MasterVolume;
			m_listSounds[i].mSpeedVolume = 0.0f;
		}
	}

	public void StartBackgroundMusic(AudioClip clip, bool loop = true, float volume = 1.0f, float fadeInTime = 0.0f)
	{
		if ( clip == null )
			return;

		// reset value for new background music
		mNewBackgroundMusicClip = null;

		mBackgroundMusic.Stop();
		PlayRCAudioSource( mBackgroundMusic, clip, loop, volume, fadeInTime );
	}

	public void StopBackgroundMusic(float fadeOutTime = 0.0f)
	{
		// reset value for new background music
		mNewBackgroundMusicClip = null;

		if ( !mBackgroundMusic.mAudio.isPlaying )
			return;
		
		if ( mBackgroundMusic.mAudio.volume <= 0.0f )
		{
			mBackgroundMusic.Stop();
			return;
		}
		
		if ( fadeOutTime == 0.0f )
			mBackgroundMusic.Stop();
		else
		{
			mBackgroundMusic.mTargetVolume = 0.0f;
			mBackgroundMusic.mSpeedVolume = -1.0f * mBackgroundMusic.mAudio.volume / fadeOutTime;
		}
	}

	public void ContinueBackgroundMusic(float fadeInTime = 0.0f)
	{
		StartBackgroundMusic( mBackgroundMusic.mAudio.clip, true, mBackgroundMusic.mTargetVolume, fadeInTime );
	}

	public void ChangeBackgroundMusic(AudioClip clip, float fadeOutTime = 0.0f, float fadeInTime = 0.0f, bool loop = true, float volume = 1.0f)
	{
		if ( clip == null )
			return;

		if ( mBackgroundMusic.mAudio.isPlaying )
		{
			StopBackgroundMusic( fadeOutTime );

			mNewBackgroundMusicClip = clip;
			mNewBackgroundMusicFadeInTime = fadeInTime;
			mNewBackgroundMusicLoop = loop;
			mNewBackgroundMusicVolume = volume;
		} else
		{
			StartBackgroundMusic( clip, loop, volume, fadeInTime );
		}
	}

	public int PlaySound(AudioClip clip, bool loop = false, float volume = 1.0f, float fadeInTime = 0.0f)
	{
		if ( clip == null )
			return -1;

		if ( AllSoundsAreOff )
			return -1;

		// get the first available AudioSource from the list m_listSounds
		for(int i=0; i<m_listSounds.Count; i++)
		{
			if ( !m_listSounds[i].mAudio.isPlaying )
			{
				PlayRCAudioSource( m_listSounds[i], clip, loop, volume, fadeInTime );
				return i;
			}
		}

		// if we get here it means that we did not found any vailable AudioSource from the list so we need to add a new one
		RCAudioSource newRCAS = AddRCAudioSource();
		m_listSounds.Add( newRCAS );
		PlayRCAudioSource( m_listSounds[m_listSounds.Count - 1], clip, loop, volume, fadeInTime );
		return m_listSounds.Count - 1;
	}

	// soundId = returned from PlaySound
	public void StopSound(int soundId, float fadeOutTime = 0.0f)
	{
		if ( soundId == -1 )
			return;

		if ( !m_listSounds[soundId].mAudio.isPlaying )
			return;

		if ( m_listSounds[soundId].mAudio.volume <= 0.0f )
		{
			m_listSounds[soundId].Stop();
			return;
		}

		if ( fadeOutTime == 0.0f )
			m_listSounds[soundId].Stop();
		else
		{
			m_listSounds[soundId].mTargetVolume = 0.0f;
			m_listSounds[soundId].mSpeedVolume = -1.0f * m_listSounds[soundId].mAudio.volume / fadeOutTime;
		}
	}

	public void StopAllSounds()
	{
		for(int i=0; i<m_listSounds.Count; i++)
			m_listSounds[i].Stop();
	}

	public void TurnOffAllSounds()
	{
		StopAllSounds();
		AllSoundsAreOff = true;
	}

	public void TurnOnAllSounds()
	{
		AllSoundsAreOff = false;
	}

	#endregion


	RCAudioSource AddRCAudioSource()
	{
		RCAudioSource newRCAS = new RCAudioSource();
		newRCAS.mAudio = gameObject.AddComponent<AudioSource>();
		return newRCAS;
	}

	void PlayRCAudioSource(RCAudioSource audio, AudioClip clip, bool loop, float volume, float fadeInTime)
	{
		audio.mAudio.clip = clip;
		audio.mAudio.loop = loop;

		if ( fadeInTime == 0.0f )
		{
			audio.mAudio.volume = volume * MasterVolume;
			audio.mSpeedVolume = 0.0f;
		} else
		{
			audio.mAudio.volume = 0.0f;
			audio.mTargetVolume = volume * MasterVolume;
			audio.mSpeedVolume = audio.mTargetVolume / fadeInTime;
		}

		audio.mAudio.Play();
	}

	// get instance
	public static RSAudioManager Instance
	{
		get
		{
			if (instance == null)
				Debug.Log("--- RSAudioManager instance should not be nulll ---");
			return instance;
		}
	}
}
