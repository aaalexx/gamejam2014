﻿using UnityEngine;
using System.Collections;
using System;

public enum TileColor : int
{
    RED = 0,
    GREEN,
    BLUE,
    YELLOW,
    PURPLE,
    SIZE,
    BEACH_BALL,
    POWERUP_NONE,
    POWERUP_1,
    POWERUP_2,
    POWERUP_3,
    POWERUP_4
}

public class CTile : IComparable<CTile>
{
    public TileColor Color {
        get { return mColor; }
    }

    public int X {
        get { return (int) mPosition.x; }
    }

    public int Y {
        get { return (int) mPosition.y; }
    }

    private TileColor mColor;
    private Vector2 mPosition;
    private bool mMarkForDelete;
    private bool mMarkForNew;
    private TileColor mShouldBePowerUp;

    public CTile(TileColor color, Vector2 position)
    {
        mColor = color;
        mPosition = position;
        mMarkForDelete = false;
        mMarkForNew = false;
        mShouldBePowerUp = TileColor.POWERUP_NONE;
    }

    public int CompareTo(CTile other)
    {
        if (other == null)
        {
            return 1;
        }
        
        //Return the difference in power.
        return mColor - other.mColor;
    }

    public void MarkForDelete(bool mark)
    {
        mMarkForDelete = mark;
    }

    public void MarkForNew(bool mark)
    {
        mMarkForNew = mark;
    }

    public bool IsMarkForDelete()
    {
        return mMarkForDelete;
    }

    public bool IsMarkForNew()
    {
        return mMarkForNew;
    }

    public void SetColor(TileColor color)
    {
        mColor = color;
    }

    public void SetShouldBePowerUp(TileColor powerup)
    {
        mShouldBePowerUp = powerup;
    }

    public TileColor ShouldBePowerUp()
    {
        return mShouldBePowerUp;
    }
}
