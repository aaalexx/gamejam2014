﻿using UnityEngine;
using System.Collections;

public class STile : MonoBehaviour
{
    public int Row = -1;
    public int Col = -1;
    public bool IsEmptyTile = false;
    public Texture2D EmptyTexture;
    public bool IsPressed = false;

    // Use this for initialization
    void Start()
    {
    }
    
    // Update is called once per frame
    void Update()
    {
        if (IsEmptyTile)
        {
            if (RCCustomInput.IsClickPressed() && guiTexture.HitTest(RCCustomInput.GetCursorPostion_V3()))
            {
                guiTexture.enabled = true;
            }
            else
            {
                guiTexture.enabled = false;
            }
        }
    }
}
