﻿using UnityEngine;
using System.Collections;

public class SHud : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.FindChild("Objective").FindChild("Text").guiText.text = SBoard.mObjectives + "/" + SBoard.mMaxObjectives;
        transform.FindChild("Score").FindChild("Text").guiText.text = SBoard.mScore.ToString();
        transform.FindChild("Moves").FindChild("Number").guiText.text = SBoard.mMoves.ToString();
    }
}
