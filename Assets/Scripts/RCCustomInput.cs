﻿using UnityEngine;
using System.Collections;

public class RCCustomInput
{
    public static float GetCursorPostion_X()
    {
        #if ( UNITY_IPHONE || UNITY_ANDROID ) && !UNITY_EDITOR
        return Input.GetTouch(0).position.x;
        #else
        return Input.mousePosition.x;
        #endif
    }
    
    public static float GetCursorPostion_Y()
    {
        #if ( UNITY_IPHONE || UNITY_ANDROID ) && !UNITY_EDITOR
        return Input.GetTouch(0).position.y;
        #else
        return Input.mousePosition.y;
        #endif
    }
    
    public static Vector2 GetCursorPosition_V2()
    {
        Vector2 pos = Vector2.zero;
        pos.x = GetCursorPostion_X();
        pos.y = GetCursorPostion_Y();
        return pos;
    }

    public static Vector3 GetCursorPostion_V3()
    {
        Vector3 pos = Vector3.zero;
        pos.x = GetCursorPostion_X();
        pos.y = GetCursorPostion_Y();
        return pos;
    }
    
    public static bool IsClickPressed(bool bJustOneClick = true)
    {
        #if ( UNITY_IPHONE || UNITY_ANDROID ) && !UNITY_EDITOR
        if ( bJustOneClick )
        {
            if ( Input.touchCount == 1 )
                return true;
            else
                return false;
        } else
        {
            if ( Input.touchCount >= 1 )
                return true;
            else
                return false;
        }
        #else
        if (bJustOneClick)
        {
            if (Input.GetMouseButton(0) && !Input.GetMouseButton(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endif
    }

    public static bool IsPressed()
    {
        bool flag = false;

//#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
//        flag = Input.GetTouch(0).phase == TouchPhase.Begin;
//#else
        flag = Input.GetMouseButtonDown(0);
//#endif

        return flag;
    }

    public static bool IsReleased()
    {
        bool flag = false;

//#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
//        flag = Input.GetTouch(0).phase == TouchPhase.Ended;
//#else
        flag = Input.GetMouseButtonUp(0);
//#endif

        return flag;

    }

    public static bool IsMoving()
    {
        bool flag = false;
        
//        #if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
//        flag = Input.GetTouch(0).phase == TouchPhase.Ended;
//        #else
//        flag = Input.GetMouseButtonDown(0) && Input.GetAxis("Vertical")
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("v: " + Input.GetAxis("Mouse X"));
            Debug.Log("h: " + Input.GetAxis("Mouse Y"));
        }
//        #endif
        
        return flag;
        
    }
}
